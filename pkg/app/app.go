package app

import (
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/contrib/propagators/b3"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.uber.org/zap"

	"google.golang.org/grpc"
)

type AppOption func(*App)

// App is the main application object
type App struct {
	logger *zap.Logger

	httpHost string
	httpPort int
	grpcHost string
	grpcPort int

	grpcSvr *grpc.Server
	httpSvr *runtime.ServeMux
}

// NewApp creates a new App object
func NewApp(opt ...AppOption) *App {
	const (
		httpHost = "0.0.0.0"
		httpPort = 8080
		grpcHost = "0.0.0.0"
		grpcPort = 5000
	)
	app := &App{
		logger:   zap.NewNop(),
		grpcSvr:  grpc.NewServer(),
		httpSvr:  runtime.NewServeMux(),
		httpHost: httpHost,
		httpPort: httpPort,
		grpcHost: grpcHost,
		grpcPort: grpcPort,
	}
	for _, o := range opt {
		o(app)
	}
	return app
}

// WithGRPCServer sets the gRPC server for the app
func WithGRPCServer(svr *grpc.Server) AppOption {
	return func(a *App) {
		a.grpcSvr = svr
	}
}

// WithHTTPServer sets the HTTP server for the app
func WithHTTPServer(svr *runtime.ServeMux) AppOption {
	return func(a *App) {
		a.httpSvr = svr
	}
}

// WithLogger sets the logger for the app
func WithLogger(logger *zap.Logger) AppOption {
	return func(a *App) {
		a.logger = logger
	}
}

// WithHTTPHost sets the HTTP host for the app
func WithHTTPHost(host string) AppOption {
	return func(a *App) {
		a.httpHost = host
	}
}

// WithHTTPPort sets the HTTP port for the app
func WithHTTPPort(port int) AppOption {
	return func(a *App) {
		a.httpPort = port
	}
}

// WithGRPCHost sets the gRPC host for the app
func WithGRPCHost(host string) AppOption {
	return func(a *App) {
		a.grpcHost = host
	}
}

// WithGRPCPort sets the gRPC port for the app
func WithGRPCPort(port int) AppOption {
	return func(a *App) {
		a.grpcPort = port
	}
}

// Run runs the application
func (a *App) RunGRPC() error {
	a.logger.Debug("starting GRPC server", zap.String("host", a.grpcHost), zap.Int("port", a.grpcPort))

	// check if grpc server is nil
	if a.grpcSvr == nil {
		a.logger.Fatal("grpc server is nil")
		return fmt.Errorf("grpc server is nil")
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", a.grpcHost, a.grpcPort))
	if err != nil {
		a.logger.Fatal("failed to start app ", zap.Error(err))
		return err
	}

	if err := a.grpcSvr.Serve(lis); err != nil {
		a.logger.Fatal("failed to start app ", zap.Error(err))
		return err
	}
	return nil
}

// run HTTP server
func (a *App) RunHTTP() error {
	a.logger.Debug("starting HTTP server", zap.String("host", a.httpHost), zap.Int("port", a.httpPort))
	// check if http server is nil
	if a.httpSvr == nil {
		a.logger.Fatal("http server is nil")
		return fmt.Errorf("http server is nil")
	}

	if err := http.ListenAndServe(
		fmt.Sprintf("%s:%d", a.httpHost, a.httpPort),
		//add otl to http requests
		// zap logger is used to log the request

		NewZapLoggerHandler(
			a.logger,
			otelhttp.NewHandler(
				a.httpSvr, "http-server",
				otelhttp.WithMessageEvents(otelhttp.ReadEvents, otelhttp.WriteEvents),
				otelhttp.WithPropagators(b3.New(b3.WithInjectEncoding(b3.B3MultipleHeader|b3.B3SingleHeader))),
			),
		),
	); err != nil {
		a.logger.Fatal("failed to start app ", zap.Error(err))
		return err
	}
	return nil

}

type ZapLoggerHandler struct {
	logger  *zap.Logger
	handler http.Handler
}

func (h ZapLoggerHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	// get request id from context
	lrw := NewLoggingResponseWriter(w)

	defer func() {
		// get request host

		host := r.Proto + "://" + r.Host

		reqID := r.Header.Get("X-Request-Id")
		ip := r.Header.Get("X-Forwarded-For")
		if ip == "" {
			ip = strings.Split(r.RemoteAddr, ":")[0]
		}

		respCode := lrw.StatusCode()
		h.logger.Info(
			"request",
			zap.Any("host", host),
			zap.String("method", r.Method),
			zap.String("url", r.URL.String()),
			zap.String("ip", ip),
			zap.String("requestID", reqID),
			zap.Int("responseCode", respCode),
			zap.Duration("duration", time.Since(start)),
		)
	}()

	h.handler.ServeHTTP(lrw, r)

}

func NewZapLoggerHandler(logger *zap.Logger, handler http.Handler) http.Handler {
	return ZapLoggerHandler{
		logger:  logger,
		handler: handler,
	}

}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func (w *loggingResponseWriter) WriteHeader(code int) {
	w.statusCode = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *loggingResponseWriter) StatusCode() int {
	return w.statusCode
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}
