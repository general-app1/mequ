package main

import (
	"fmt"

	app "gitlab.com/general-app1/mequ/pkg/app"
	"go.uber.org/zap"
)

func main() {
	fmt.Println("Hello, World!")
	a := app.NewApp(
		app.WithGRPCPort(8000),
		app.WithHTTPPort(8888),
		app.WithLogger(zap.NewExample()),
	)
	go a.RunHTTP()
	a.RunGRPC()

}
